/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Jaume Nin <jaume.nin@cttc.cat>
 *
 *             Lorenzo Carl <lorenzo.carl9@gmail.com>
 */

#include "ns3/lte-helper.h"
#include "ns3/epc-helper.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/config-store.h"

#include <iomanip>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
//#include "ns3/gtk-config-store.h"

using namespace ns3;

/**
 * Sample simulation script for LTE, EPC and MBMS. It instantiates one eNodeB,
 * attaches several UEs to the eNodeB starts a flow for each UE to and from a remote host.
 * At the same time a multicast stream flow to each UE from a remote host
 * It also  starts yet another flow between each UE pair.
 */
NS_LOG_COMPONENT_DEFINE ("EpcFirstExample");



int
main (int argc, char *argv[])
{

  //LogComponentEnable("LteRlcUm", LOG_LEVEL_LOGIC);
  //LogComponentEnable("LteUeMac", LOG_LEVEL_ALL);
  //LogComponentEnable("LteEnbMac", LOG_LEVEL_ALL);
  //LogComponentEnableAll(LOG_LEVEL_ALL);
  uint16_t numberOfNodes = 1;
  uint16_t numberOfUes = 5;
  double simTime = 5.0;
  double distance = 10.0;
  double interPacketInterval = 100;

  // Command line arguments
  CommandLine cmd;
  cmd.AddValue ("numberOfNodes", "Number of eNodeBs + UE pairs", numberOfNodes);
  cmd.AddValue ("simTime", "Total duration of the simulation [s])", simTime);
  cmd.AddValue ("distance", "Distance between eNBs [m]", distance);
  cmd.AddValue ("interPacketInterval", "Inter packet interval [ms])", interPacketInterval);
  cmd.Parse (argc, argv);

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<EpcHelper>  epcHelper = CreateObject<EpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");

  ConfigStore inputConfig;
  inputConfig.ConfigureDefaults ();

  // parse again so you can override default values from the command line
  cmd.Parse (argc, argv);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create the Internet
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // interface 0 is localhost, 1 is the p2p device
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer ueNodes;
  NodeContainer enbNodes;
  enbNodes.Create (numberOfNodes);
  ueNodes.Create (numberOfUes);

  // Install Mobility Model
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  for (uint16_t i = 0; i < numberOfUes; i++)
    {
      positionAlloc->Add (Vector (distance * i, 0, 0));
    }
  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (enbNodes);
  mobility.Install (ueNodes);

  // Install LTE Devices to the nodes
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install the IP stack on the UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));
  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }

  // Attach one UE per eNodeB
  for (uint16_t i = 0; i < numberOfUes; i++)
    {
      lteHelper->Attach (ueLteDevs.Get (i), enbLteDevs.Get (0));
      // side effect: the default EPS bearer will be activated
    }


  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1234;
  uint16_t ulPort = 2000;
  uint16_t otherPort = 3000;
  ApplicationContainer clientApps;
  ApplicationContainer serverApps;
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      ++ulPort;
      ++otherPort;
      PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), dlPort));
      PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), ulPort));
      PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), otherPort));
      serverApps.Add (dlPacketSinkHelper.Install (ueNodes.Get (u)));
      serverApps.Add (ulPacketSinkHelper.Install (remoteHost));
      serverApps.Add (packetSinkHelper.Install (ueNodes.Get (u)));

      UdpClientHelper dlClient (ueIpIface.GetAddress (u), dlPort);
      dlClient.SetAttribute ("Interval", TimeValue (MilliSeconds (interPacketInterval)));
      dlClient.SetAttribute ("MaxPackets", UintegerValue (1000000));

      UdpClientHelper ulClient (remoteHostAddr, ulPort);
      ulClient.SetAttribute ("Interval", TimeValue (MilliSeconds (interPacketInterval)));
      ulClient.SetAttribute ("MaxPackets", UintegerValue (1000000));

      UdpClientHelper client (ueIpIface.GetAddress (u), otherPort);
      client.SetAttribute ("Interval", TimeValue (MilliSeconds (interPacketInterval)));
      client.SetAttribute ("MaxPackets", UintegerValue (1000000));

      clientApps.Add (dlClient.Install (remoteHost));
      clientApps.Add (ulClient.Install (ueNodes.Get (u)));
      if (u + 1 < ueNodes.GetN ())
        {
          clientApps.Add (client.Install (ueNodes.Get (u + 1)));
        }
      else
        {
          clientApps.Add (client.Install (ueNodes.Get (0)));
        }
    }
  serverApps.Start (Seconds (0.01));
  clientApps.Start (Seconds (0.01));

  /**
   * TEST!!!
   *
   * Attivazione Bearer MBMS
   */
  Ptr<EpcTft> tft = Create<EpcTft> ();
  EpcTft::PacketFilter pf;
  pf.localPortStart = 1240;
  pf.localPortEnd = 1240;
  tft->Add (pf);

  Ipv4Address mbmsAddress ("225.1.2.4");
  lteHelper->ActivateEpsBearerMbms (enbLteDevs, ueLteDevs, EpsBearer (EpsBearer::GBR_CONV_VIDEO, true), tft, mbmsAddress);
  //Porta traffico multicast
  uint16_t multicastPort = 1234;       // Discard port (RFC 863)


  //Definizione indirizzi multicast
  Ipv4Address multicastSource ("1.0.0.1");
  std::set<Ipv4Address> multicastAddress;
  multicastAddress.insert ("225.1.2.4");


  Ipv4StaticRoutingHelper multicast;

  //Configurazione multicast router su PGW
  Ptr<Node> multicastRouter = pgw;       // The node in question

  Ptr<NetDevice> inputIf = pgw->GetDevice (2);      //The input NetDevice
  NetDeviceContainer outputDevices;       // A container of output NetDevices
  outputDevices.Add (enbLteDevs);      // (we only need one NetDevice here)
  std::set<Ipv4Address>::iterator iter;
  for (iter = multicastAddress.begin (); iter != multicastAddress.end ();
       ++iter)
    {

      multicast.AddMulticastRoute (multicastRouter, multicastSource, *iter,
                                   inputIf, pgw->GetDevice (1));
      // 2) Set up a default multicast route on the sender remoteHost
      Ptr<Node> sender = remoteHost;
      Ptr<NetDevice> senderIf = internetDevices.Get (1);
      //multicast.SetDefaultMulticastRoute(sender, senderIf);
      remoteHostStaticRouting->SetDefaultMulticastRoute (1);
    }


  //Install OnOff Application
  ApplicationContainer onOffApp;
  Ipv4Address multicastGroup ("225.1.2.4");
  OnOffHelper onOffHelper ("ns3::UdpSocketFactory",
                           Address (InetSocketAddress (multicastGroup, multicastPort)));
  onOffHelper.SetAttribute ("OnTime",  StringValue ("ns3::ConstantRandomVariable[Constant=40]"));
  onOffHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
  onOffHelper.SetAttribute ("DataRate", DataRateValue (DataRate ("100kbps")));
  onOffHelper.SetAttribute ("PacketSize", UintegerValue (500));

  onOffApp.Add (onOffHelper.Install (remoteHost));
  onOffApp.Start (Seconds (1.0));

    //Flow Monitor MBMS
  MbmsMonitorHelper fl;
  Ptr<MbmsMonitor> monitor;
  //Install MBMS monitor on UEs
  for (uint16_t j = 0; j < numberOfUes; j++)
    {
      monitor = fl.InstallUes (ueNodes.Get (j));
    }

//Set IP multicast address to monitor
  std::map<Ipv4Address, uint16_t> m_Ipv4Mrnti;
  m_Ipv4Mrnti.insert (std::pair <Ipv4Address, uint16_t > ("225.1.2.4", 1025));
  monitor = fl.InstallSourcesMbms (remoteHostContainer, m_Ipv4Mrnti);

//Set Attributes
  monitor->SetAttribute ("MaxPerHopDelay", TimeValue (Seconds (1)));
  monitor->SetAttribute ("DelayBinWidth", DoubleValue (0.001));
  monitor->SetAttribute ("JitterBinWidth", DoubleValue (0.001));
  monitor->SetAttribute ("PacketSizeBinWidth", DoubleValue (20));

//Set Group Multicast - to simplify
  std::set <uint16_t> mrntiMap1;
  uint16_t sizeNode1  = ueNodes.GetN ();
  for (uint16_t i = 0; i < sizeNode1; i++)
    {
      mrntiMap1.insert (i);
    }
  monitor = fl.SetMapMrntiUe (1025, mrntiMap1);
  monitor->SetMapMrntiUe (1025, mrntiMap1);


  lteHelper->EnableTraces ();
  // Uncomment to enable PCAP tracing
  p2ph.EnablePcapAll ("lena-epc-first");

  Simulator::Stop (Seconds (simTime));
  Simulator::Run ();

  /*GtkConfigStore config;
  config.ConfigureAttributes();*/
  monitor->SerializeToXmlFile ("resultMbms.xml", true, true);
  Simulator::Destroy ();
  return 0;

}

