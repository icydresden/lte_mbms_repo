/*
 * lte-mrnti-tag.cc
 *
 *  Created on: 19/nov/2012
 *      Author: lorenzo
 */

#include "ns3/lte-mrnti-tag.h"


namespace ns3{

NS_OBJECT_ENSURE_REGISTERED (LteMrntiTag);

LteMrntiTag::LteMrntiTag ()
{
}

void
LteMrntiTag::SetMrnti (uint16_t mrnti)
{
  m_mrnti = mrnti;
}

uint16_t
LteMrntiTag::GetMrnti (void) const
{
  return m_mrnti;
}

TypeId
LteMrntiTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::LteMrntiTag")
    .SetParent<Tag> ()
    .AddConstructor<LteMrntiTag> ()
  ;
  return tid;
}
TypeId
LteMrntiTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

uint32_t
LteMrntiTag::GetSerializedSize (void) const
{
  return 2;
}
void
LteMrntiTag::Serialize (TagBuffer i) const
{
  i.WriteU16 (m_mrnti);
}
void
LteMrntiTag::Deserialize (TagBuffer i)
{
  m_mrnti = i.ReadU16 ();
}
void
LteMrntiTag::Print (std::ostream &os) const
{
  os << "SDU Status=" << (uint32_t) m_mrnti;
}

}


