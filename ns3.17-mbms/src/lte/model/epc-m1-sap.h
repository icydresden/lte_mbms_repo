/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */
#ifndef EPC_M1_SAP_H
#define EPC_M1_SAP_H

#include <ns3/ptr.h>
#include <ns3/object.h>

namespace ns3 {

class EpcM1Sap
{
public:
	virtual ~EpcM1Sap ();
};


/**
 * \ingroup lte
 *
 * MBMS-GW side of the M1 Service Access Point (SAP), provides the MBMS-GW
 * methods to be called when an M1 message is received by the MBMS-GW.
 */
class EpcM1SapMbmsGw: public EpcM1Sap
{
public:
	//TO-DO
};

/**
 * \ingroup lte
 *
 * ENB side of the M1 Service Access Point (SAP), provides the ENB
 * methods to be called when an M1 message is received by the ENB.
 */
class EpcM1SapEnb: public EpcM1Sap
{
public:
	//TO-DO
};

/**
 * Template for the implementation of the EpcM1SapMbmsGw as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcM1SapMbmsGw : public EpcM1SapMbmsGw
{
public:
  MemberEpcM1SapMbmsGw (C* owner);

  //Inherited from EpcM1SapMbmsGw

private:
  MemberEpcM1SapMbmsGw ();
  C* m_owner;
};


/**
 * Template for the implementation of the EpcS1apSapMme as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcM1SapEnb : public EpcM1SapEnb
{
public:
  MemberEpcM1SapEnb (C* owner);

  //Inherited from EpcM1SapEnb

private:
  MemberEpcM1SapEnb ();
  C* m_owner;
};



} //namespace ns3

#endif /* EPC_M1_SAP_H_ */
