/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */
#ifndef EPC_M2_SAP_H
#define EPC_M2_SAP_H

#include <ns3/address.h>
#include <ns3/ptr.h>
#include <ns3/object.h>
#include <ns3/eps-bearer.h>
#include <ns3/epc-tft.h>
#include <list>

namespace ns3 {

class EpcM2Sap
{
public:
	virtual ~EpcM2Sap ();
};


/**
 * \ingroup lte
 *
 * Mce side of the M2 Service Access Point (SAP), provides the Mce
 * methods to be called when an M2 message is received by the Mce.
 */
class EpcM2SapMce: public EpcM2Sap
{
public:

  /**
    *
    * Response to the setup of a MBMS Data Radio Bearer
    */
   virtual void MbmsSessionStartResponse (uint16_t tmgi) = 0;
};

/**
 * \ingroup lte
 *
 * ENB side of the M2 Service Access Point (SAP), provides the ENB
 * methods to be called when an M2 message is received by the ENB.
 */
class EpcM2SapEnb: public EpcM2Sap
{
public:
  /**
   * Parameters passed to SessionStartRequest - see 36.443 9.1.2
   *
   */
  struct MbmsBearerSessionStartRequestParameters
  {
    uint16_t tmgi;
    uint8_t mbmsSessionIdentifier;
    uint16_t mbmsServiceArea;
    Ipv4Address multicastAddress;
    uint32_t gtpTeid;
  };

  /**
   *
   * Request the setup for a MBMS Data Radio Bearer
   */
  virtual void MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params) = 0;
};

/**
 * Template for the implementation of the EpcM2SapMce as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcM2SapMce : public EpcM2SapMce
{
public:
  MemberEpcM2SapMce (C* owner);

  //Inherited from EpcM2sapMce
  virtual void MbmsSessionStartResponse (uint16_t tmgi);

private:
  MemberEpcM2SapMce ();
  C* m_owner;

};

template <class C>
MemberEpcM2SapMce<C>::MemberEpcM2SapMce (C* owner)
  :m_owner (owner)
{
}

template <class C>
MemberEpcM2SapMce<C>::MemberEpcM2SapMce ()
{
}

template <class C>
void MemberEpcM2SapMce<C>::MbmsSessionStartResponse (uint16_t tmgi)
{
  m_owner->DoMbmsSessionStartResponse (tmgi);
}

/**
 * Template for the implementation of the EpcM2SapEnb as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcM2SapEnb : public EpcM2SapEnb
{
public:
  MemberEpcM2SapEnb (C* owner);

  //Inherited from EpcM2SapEnb
  virtual void MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params);

private:
  MemberEpcM2SapEnb ();
  C* m_owner;

};

template <class C>
MemberEpcM2SapEnb<C>::MemberEpcM2SapEnb (C* owner)
  :m_owner(owner)
 {
 }

template <class C>
MemberEpcM2SapEnb<C>::MemberEpcM2SapEnb ()
{
}

template <class C>
void MemberEpcM2SapEnb<C>::MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters msg)
{
  m_owner->DoMbmsSessionStartRequest (msg);
}

} //namespace ns3

#endif /* EPC_M2_SAP_H_ */
