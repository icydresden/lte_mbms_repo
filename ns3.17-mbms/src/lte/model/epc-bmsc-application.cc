/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */

#include <ns3/log.h>
#include "ns3/epc-bmsc-application.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("EpcBmScApplication");

TypeId
EpcBmScApplication::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::EpcBmScApplication")
    .SetParent<Object> ();
  return tid;
}

void
EpcBmScApplication::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  delete m_sgmbBmsc;
}

EpcBmScApplication::EpcBmScApplication()
: m_lastAllocatedTmgi (0)
{
  m_sgmbBmsc = new MemberEpcSgmbSapBmsc<EpcBmScApplication> (this);
}

EpcBmScApplication::~EpcBmScApplication (void)
{
  NS_LOG_FUNCTION (this);
}


void
EpcBmScApplication::SetSgmbSapMbmsGw (EpcSgmbSapMbmsGw * s)
{
  NS_LOG_FUNCTION (this);
  m_sgmbMbmsGw = s;
}

EpcSgmbSapBmsc*
EpcBmScApplication::GetSgmbSapBmsc ()
{
  NS_LOG_FUNCTION (this);
  return m_sgmbBmsc;
}

uint16_t
EpcBmScApplication::StartMbmsSessionRequest (Ipv4Address multiAddress, NetDeviceContainer enbDevices, Ptr<EpcTft> tft, EpsBearer bearer)
{
  NS_LOG_FUNCTION (this);
  uint16_t tmgi = ++m_lastAllocatedTmgi;

  EpcSgmbSapMbmsGw::MbmsBearerSessionStartRequestParameters params;
  params.tmgi = tmgi;
  params.mbmsServiceArea = 0; // to be implemented
  params.mbmsSessionIdentifier = 0; // to be implemented
  params.multicastAddress = multiAddress;
  params.gtpTeid = 0; // to be implemented
  params.bearerLevelQos = bearer;
  params.tft = tft;
  m_sgmbMbmsGw->MbmsSessionStartRequest(params);

  return tmgi;
}

void
EpcBmScApplication::DoMbmsSessionStartResponse (uint16_t tmgi)
{
  NS_LOG_FUNCTION (this);
}

}


