/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *      Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */



#include <ns3/log.h>

#include "epc-mce.h"

NS_LOG_COMPONENT_DEFINE ("EpcMce");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (EpcMce);

EpcMce::EpcMce ()
  : m_m2SapEnb (0),
    m_m3SapMme (0)
{
  NS_LOG_FUNCTION (this);
  m_m2SapMce = new MemberEpcM2SapMce<EpcMce> (this);
  m_m3SapMce = new MemberEpcM3SapMce<EpcMce> (this);
}

EpcMce::~EpcMce ()
{
  NS_LOG_FUNCTION (this);
}

void
EpcMce::DoDispose()
{
  NS_LOG_FUNCTION (this);
  delete m_m2SapMce;
  delete m_m3SapMce;
}

TypeId
EpcMce::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::EpcMce")
    .SetParent<Object> ()
    .AddConstructor<EpcMce> ()
    ;
  return tid;
}

void
EpcMce::SetM2SapEnb (EpcM2SapEnb * s)
{
  m_m2SapEnb = s;
}

EpcM2SapMce*
EpcMce::GetM2SapMce ()
{
  return m_m2SapMce;
}

void
EpcMce::SetM3SapMme (EpcM3SapMme * s)
{
  m_m3SapMme = s;
}

EpcM3SapMce*
EpcMce::GetM3SapMce()
{
  return m_m3SapMce;
}

void
EpcMce::AddEnb(uint16_t cellId, EpcM2SapEnb* enbM2Sap)
{
  NS_LOG_FUNCTION (this << cellId);
  Ptr<EnbInfo> enbInfo = Create<EnbInfo> ();
  enbInfo->gci = cellId;
  enbInfo->m2SapEnb = enbM2Sap;
  m_enbInfoMap[cellId] = enbInfo;
}

void
EpcMce::DoMbmsSessionStartRequest (EpcM3SapMce::MbmsBearerSessionStartRequestParameters msg)
{
  NS_LOG_FUNCTION (this);
}

void
EpcMce::DoMbmsSessionStartResponse (uint16_t tmgi)
{
  NS_LOG_FUNCTION (this);
}

}

