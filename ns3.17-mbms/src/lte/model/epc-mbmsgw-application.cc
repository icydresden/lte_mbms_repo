/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *      Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */


#include "ns3/log.h"
#include "epc-mbmsgw-application.h"
#include "ns3/inet-socket-address.h"
#include <ns3/lte-enb-net-device.h>
#include <ns3/lte-ue-net-device.h>
#include <ns3/lte-ue-phy.h>
#include <ns3/lte-ue-mac.h>
#include <ns3/lte-ue-rrc.h>
#include <ns3/lte-enb-rrc.h>
#include <ns3/inet-socket-address.h>
#include <ns3/mac48-address.h>
#include <ns3/ipv4-address.h>
#include "ns3/epc-gtpu-header.h"
#include "ns3/epc-enb-application.h"
#include "ns3/uinteger.h"
#include "ns3/ipv4.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("EpcMbmsgwApplication");

/////////////////////////
// MbmsInfo
/////////////////////////

EpcMbmsgwApplication::MbmsInfo::MbmsInfo ()
{
  NS_LOG_FUNCTION (this);
}

void
EpcMbmsgwApplication::MbmsInfo::SetEnbAddress (Ipv4Address enbAddr)
{
  m_enbAddr = enbAddr;
}

Ipv4Address
EpcMbmsgwApplication::MbmsInfo::GetEnbAddress ()
{
  return m_enbAddr;
}

EpcMbmsgwApplication::EpsFlowMbmsId_t::EpsFlowMbmsId_t ()
{
}

EpcMbmsgwApplication::EpsFlowMbmsId_t::EpsFlowMbmsId_t (const uint16_t a, const uint8_t b)
  : m_mrnti (a),
    m_bid (b)
{
}

bool
operator == (const EpcMbmsgwApplication::EpsFlowMbmsId_t &a, const EpcMbmsgwApplication::EpsFlowMbmsId_t &b)
{
  return ( (a.m_mrnti == b.m_mrnti) && (a.m_bid == b.m_bid) );
}

bool
operator < (const EpcMbmsgwApplication::EpsFlowMbmsId_t& a, const EpcMbmsgwApplication::EpsFlowMbmsId_t& b)
{
  return ( (a.m_mrnti < b.m_mrnti) || ( (a.m_mrnti == b.m_mrnti) && (a.m_bid < b.m_bid) ) );
}


TypeId
EpcMbmsgwApplication::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::EpcMbmsgwApplication")
    .SetParent<Object> ();
  return tid;
}

EpcMbmsgwApplication::EpcMbmsgwApplication (Ptr<Socket> sgimbSocket, Ptr<Socket> m1Socket)
  : m_sgimbSocket (sgimbSocket),
    m_m1Socket (m1Socket),
    m_gtpuUdpPort (2152),
    m_bearerCounter (5),
    m_lastAllocatedMrnti (1 << 10),
    m_cTeidCounter (0)
{
  NS_LOG_FUNCTION (this << sgimbSocket << m1Socket);
  m_sgimbSocket->SetRecvCallback (MakeCallback (&EpcMbmsgwApplication::RecvFromSgimbSocket, this));
  m_sgmbMbmsGw = new MemberEpcSgmbSapMbmsGw<EpcMbmsgwApplication>(this);
  m_smSapMbmsGw = new MemberEpcSmSapMbmsGw<EpcMbmsgwApplication>(this);
}

EpcMbmsgwApplication::~EpcMbmsgwApplication (void)
{
  NS_LOG_FUNCTION (this);
}

void
EpcMbmsgwApplication::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_sgimbSocket = 0;
  m_m1Socket = 0;
  delete m_smSapMbmsGw;
}

void
EpcMbmsgwApplication::SetSgmbSapBmSc (EpcSgmbSapBmsc * s)
{
  NS_LOG_FUNCTION (this);
  m_sgmbBmsc = s;
}

EpcSgmbSapMbmsGw*
EpcMbmsgwApplication::GetSgmbMbmsGw ()
{
  NS_LOG_FUNCTION (this);
  return m_sgmbMbmsGw;
}

void
EpcMbmsgwApplication::SetSmSapMme (EpcSmSapMme * s)
{
  NS_LOG_FUNCTION (this);
  m_smSapMme = s;
}

EpcSmSapMbmsGw*
EpcMbmsgwApplication::GetSmMbmsGw()
{
  NS_LOG_FUNCTION (this);
  return m_smSapMbmsGw;
}

void
EpcMbmsgwApplication::SetMbmsInfoMap (Ipv4Address multiAddress, NetDeviceContainer enbDevices, NetDeviceContainer ueDevices, Ptr<EpcTft> tft, EpsBearer bearer, Ptr<EpcMme> mme)
{
  //Create MRNTI
  uint16_t mrnti = ++m_lastAllocatedMrnti;

  uint8_t bearerCounter = m_bearerCounter;
  EpsFlowMbmsId_t rbid (mrnti, bearerCounter);
  m_rbidIpv4Map[multiAddress] = rbid;

  //Create C-TEID
  uint32_t cteid = ++m_cTeidCounter;

  m_ipv4TeidMap[multiAddress] = cteid;

  //Temporary Solution for eNodeB
  for (NetDeviceContainer::Iterator j = enbDevices.Begin (); j != enbDevices.End (); ++j)
    {
      Ptr<NetDevice> enbDevice = *j;

      Ptr<LteEnbNetDevice> enbLteDevice = enbDevice->GetObject<LteEnbNetDevice> ();
      if (enbLteDevice)
        {

          Ptr<LteEnbRrc> enbRrc =  enbLteDevice->GetObject<LteEnbNetDevice> ()->GetRrc ();
          enbRrc->AddMbms (mrnti);
          enbRrc->SetupDataRadioBearerMbms (mrnti, bearer, bearerCounter, multiAddress);
          Ptr<Node> enbNode = enbDevice->GetNode ();

          Ptr<EpcEnbApplication> enbApp = enbNode->GetApplication (0)->GetObject<EpcEnbApplication> ();
          enbApp->SetupM1Bearer (cteid, mrnti, bearerCounter);

          std::map<Ipv4Address, std::set< Ptr<Node> > >::iterator it =
            m_mbmsInfoMap.find (multiAddress);
          if (it == m_mbmsInfoMap.end ())
            {
              //New entry
              m_mbmsInfoMap[multiAddress].insert (enbNode);
            }
          else
            {
              //Update
              m_mbmsInfoMap[multiAddress].insert (enbNode);
            }
        }
    }

  for (NetDeviceContainer::Iterator i = ueDevices.Begin (); i != ueDevices.End (); ++i)
    {
      Ptr<NetDevice> ueDevice = *i;
      Ptr<LteUeNetDevice> ueLteDevice = ueDevice->GetObject<LteUeNetDevice> ();
      if (ueLteDevice)
        {

          Ptr<Node> ueNode = ueDevice->GetNode ();
          Ptr<Ipv4> ueIpv4 = ueNode->GetObject<Ipv4> ();
          NS_ASSERT_MSG (ueIpv4 != 0, "UEs need to have IPv4 installed before EPS bearers can be activated");
          int32_t interface =  ueIpv4->GetInterfaceForDevice (ueDevice);
          NS_ASSERT (interface >= 0);
          NS_ASSERT (ueIpv4->GetNAddresses (interface) == 1);

          Ptr<LteUeNetDevice> ueLteDevice = ueDevice->GetObject<LteUeNetDevice> ();
          if (ueLteDevice)
            {
              Ptr<LteUeRrc> uerrc = ueLteDevice->GetRrc ();
              uerrc->AddBearerMbms (mrnti, bearerCounter, bearer);
            }
        }
    }
}



void
EpcMbmsgwApplication::RecvFromSgimbSocket (Ptr<Socket> sgimbSocket)
{
  NS_LOG_FUNCTION (this << sgimbSocket);
  NS_ASSERT (sgimbSocket == m_sgimbSocket);

  Ptr<Packet> packet = sgimbSocket->Recv ();
  GtpuHeader gtpu;
  packet->RemoveHeader (gtpu);
  //uint32_t teid = gtpu.GetTeid ();

  //Invio ai vari eNB o inserimento pacchetto nel buffer
}

void
EpcMbmsgwApplication::SendtoM1Socket (Ptr<Packet> packet,
                                      Ipv4Address enbAddr, uint32_t cteid)
{
  NS_LOG_FUNCTION (this << packet << enbAddr << cteid);

  GtpuHeader gtpu;
  gtpu.SetTeid (cteid);
  // From 3GPP TS 29.281 v10.0.0 Section 5.1
  // Length of the payload + the non obligatory GTP-U header
  gtpu.SetLength (packet->GetSize () + gtpu.GetSerializedSize () - 8);
  packet->AddHeader (gtpu);
  uint32_t flags = 0;
  m_m1Socket->SendTo (packet, flags,
                      InetSocketAddress (enbAddr, m_gtpuUdpPort));
}

void
EpcMbmsgwApplication::ReceiveMbmsData (Ptr<Packet> packet)
{
  //Gestione pacchetto

  //Get Multicast Address
  Ptr<Packet> pCopy = packet->Copy ();
  Ipv4Header ipv4Header;
  pCopy->RemoveHeader (ipv4Header);
  Ipv4Address mbmsAddress =  ipv4Header.GetDestination ();

  //Get C-TEID
  std::map<Ipv4Address, uint32_t>::iterator iter = m_ipv4TeidMap.find (mbmsAddress);
  uint32_t cteid = iter->second;
  NS_LOG_FUNCTION (this << "Packet: " << packet << " MBMS Address: " << mbmsAddress);
  std::map<Ipv4Address, std::set<Ptr<Node> > >::iterator it = m_mbmsInfoMap.find (mbmsAddress);

  if (it == m_mbmsInfoMap.end ())
    {
      NS_LOG_WARN ("Unknown MBMS Address " << mbmsAddress);
    }
  else
    {
      std::set<Ptr<Node> >::iterator iter;
      //Lettura mappa indirizzi -> invio a eNodeB (prova attraverso socket, altrimenti funzione)
      for (iter = it->second.begin (); iter != it->second.end (); ++iter)
        {
          Ptr<Node> enbNode = *iter;
          Ptr<Ipv4> enBIpv4 = enbNode->GetObject<Ipv4> ();
          NS_ASSERT_MSG (enBIpv4 != 0, "UEs need to have IPv4 installed before EPS bearers can be activated");
          Ipv4Address enBAddr = enBIpv4->GetAddress (2, 0).GetLocal ();
          NS_LOG_LOGIC (" UE IP address: " << enBAddr);
          SendtoM1Socket (packet, enBAddr, cteid);
        }
    }

}

// Sm SAP forwarded methods
void
EpcMbmsgwApplication::DoMbmsSessionStartResponse (EpcSmSapMbmsGw::MbmsSessionResponseMessage msg)
{
  NS_LOG_FUNCTION (this);
}

// SGmb SAP forwarded methods
void
EpcMbmsgwApplication::DoMbmsSessionStartRequest (EpcSgmbSapMbmsGw::MbmsBearerSessionStartRequestParameters params)
{
  NS_LOG_FUNCTION (this);
  EpcSmSapMme::CreateMbmsSessionRequestMessage msg;

  msg.tmgi = params.tmgi;
  msg.mbmsServiceArea = params.mbmsServiceArea;
  msg.mbmsSessionIdentifier = params.mbmsSessionIdentifier;
  msg.multicastAddress = params.multicastAddress;
  msg.teid = params.gtpTeid;
  msg.bearerLevelQos = params.bearerLevelQos;
  msg.tft = params.tft;

  m_smSapMme->MbmsSessionStartRequest(msg);

}


}
