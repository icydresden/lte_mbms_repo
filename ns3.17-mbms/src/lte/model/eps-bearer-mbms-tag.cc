/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011,2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carlà
 */


#include "eps-bearer-mbms-tag.h"
#include "ns3/tag.h"
#include "ns3/uinteger.h"

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (EpsBearerMbmsTag);

TypeId
EpsBearerMbmsTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::EpsBearerMbmsTag")
    .SetParent<Tag> ()
    .AddConstructor<EpsBearerMbmsTag> ()
    .AddAttribute ("M-Rnti", "The M-Rnti that indicates the UE which packet belongs",
                   UintegerValue (0),
                   MakeUintegerAccessor (&EpsBearerMbmsTag::GetMrnti),
                   MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("bid", "The EPS bearer id within the UE to which the packet belongs",
                   UintegerValue (0),
                   MakeUintegerAccessor (&EpsBearerMbmsTag::GetBid),
                   MakeUintegerChecker<uint8_t> ())
  ;
  return tid;
}

TypeId
EpsBearerMbmsTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

EpsBearerMbmsTag::EpsBearerMbmsTag ()
  : m_mrnti (0),
    m_bid (0)
{
}
EpsBearerMbmsTag::EpsBearerMbmsTag (uint16_t mrnti, uint8_t bid)
  : m_mrnti (mrnti),
    m_bid (bid)
{
}

void
EpsBearerMbmsTag::SetMrnti (uint16_t mrnti)
{
  m_mrnti = mrnti;
}

void
EpsBearerMbmsTag::SetBid (uint8_t bid)
{
  m_bid = bid;
}

uint32_t
EpsBearerMbmsTag::GetSerializedSize (void) const
{
  return 3;
}

void
EpsBearerMbmsTag::Serialize (TagBuffer i) const
{
  i.WriteU16 (m_mrnti);
  i.WriteU8 (m_bid);
}

void
EpsBearerMbmsTag::Deserialize (TagBuffer i)
{
  m_mrnti = (uint16_t) i.ReadU16 ();
  m_bid = (uint8_t) i.ReadU8 ();
}

uint16_t
EpsBearerMbmsTag::GetMrnti () const
{
  return m_mrnti;
}

uint8_t
EpsBearerMbmsTag::GetBid () const
{
  return m_bid;
}

void
EpsBearerMbmsTag::Print (std::ostream &os) const
{
  os << "mrnti=" << m_mrnti << ", bid=" << (uint16_t) m_bid;
}

} // namespace ns3
