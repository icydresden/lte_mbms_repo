// -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*-
//
// Copyright (c) 2009 INESC Porto
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation;
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Author: Gustavo J. A. M. Carneiro  <gjc@inescporto.pt> <gjcarneiro@gmail.com>
//              Lorenzo Carla' <lorenzo.carl9@gmai.com>

#ifndef MBMS_PROBE_H_
#define MBMS_PROBE_H_

#include <map>
#include <vector>

#include "ns3/simple-ref-count.h"
#include "ns3/mbms-classifier.h"
#include "ns3/nstime.h"
#include "ns3/histogram.h"
namespace ns3 {

class MbmsMonitor;

/// The MbmsProbe class is responsible for listening for packet events
/// in a specific point of the simulated space, report those events to
/// the global MbmsMonitor, and collect its own flow statistics
/// regarding only the packets that pass through that probe.
class MbmsProbe : public SimpleRefCount<MbmsProbe>
{
private:
  MbmsProbe (MbmsProbe const &);
  MbmsProbe& operator= (MbmsProbe const &);

protected:
  MbmsProbe (Ptr<MbmsMonitor> MbmsMonitor);

public:
  virtual ~MbmsProbe ();

  struct Statistics
  {
    double throughput;
    double jitter;
    double delay;
    double dropping;
  };

  struct FlowStats
  {
    /*
    FlowStats() :
                    delayFromFirstProbeSum(Seconds(0)), bytes(0), packets(0) {
    }
*/
    /// packetsDropped[reasonCode] => number of dropped packets
    std::vector<uint32_t> packetsDropped;
    /// bytesDropped[reasonCode] => number of dropped bytes
    std::vector<uint64_t> bytesDropped;
    /// divide by 'packets' to get the average delay from the
    /// first (entry) probe up to this one (partial delay)
    Time delayFromFirstProbeSum;
    /// Number of bytes seen of this flow
    uint64_t bytes;
    /// Number of packets seen of this flow
    uint32_t packets;

    /// Contains the absolute time when the first packet in the flow
    /// was transmitted, i.e. the time when the flow transmission
    /// starts
    Time timeFirstTxPacket;

    /// Contains the absolute time when the first packet in the flow
    /// was received by an end node, i.e. the time when the flow
    /// reception starts
    Time timeFirstRxPacket;

    /// Contains the absolute time when the last packet in the flow
    /// was transmitted, i.e. the time when the flow transmission
    /// ends
    Time timeLastTxPacket;

    /// Contains the absolute time when the last packet in the flow
    /// was received, i.e. the time when the flow reception ends
    Time timeLastRxPacket;

    /// Contains the sum of all end-to-end delays for all received
    /// packets of the flow.
    Time delaySum;             // delayCount == rxPackets

    /// Contains the sum of all end-to-end delay jitter (delay
    /// variation) values for all received packets of the flow.  Here
    /// we define _jitter_ of a packet as the delay variation
    /// relatively to the last packet of the stream,
    /// i.e. \f$Jitter\left\{P_N\right\} = \left|Delay\left\{P_N\right\} - Delay\left\{P_{N-1}\right\}\right|\f$.
    /// This definition is in accordance with the Type-P-One-way-ipdv
    /// as defined in IETF RFC 3393.
    Time jitterSum;             // jitterCount == rxPackets - 1

    Time lastDelay;

    /// Total number of transmitted bytes for the flow
    uint64_t txBytes;
    /// Total number of received bytes for the flow
    uint64_t rxBytes;
    /// Total number of transmitted packets for the flow
    uint32_t txPackets;
    /// Total number of received packets for the flow
    uint32_t rxPackets;

    //MRNTI
    uint16_t mrnti;

    /// Total number of packets that are assumed to be lost,
    /// i.e. those that were transmitted but have not been reportedly
    /// received or forwarded for a long time.  By default, packets
    /// missing for a period of over 10 seconds are assumed to be
    /// lost, although this value can be easily configured in runtime
    uint32_t lostPackets;

    /// Contains the number of times a packet has been reportedly
    /// forwarded, summed for all received packets in the flow
    uint32_t timesForwarded;

    /// Histogram of the packet delays
    Histogram delayHistogram;
    /// Histogram of the packet jitters
    Histogram jitterHistogram;
    /// Histogram of the packet sizes
    Histogram packetSizeHistogram;

    /// This attribute also tracks the number of lost packets and
    /// bytes, but discriminates the losses by a _reason code_.  This
    /// reason code is usually an enumeration defined by the concrete
    /// MbmsProbe class, and for each reason code there may be a
    /// vector entry indexed by that code and whose value is the
    /// number of packets or bytes lost due to this reason.  For
    /// instance, in the Ipv4MbmsProbe case the following reasons are
    /// currently defined: DROP_NO_ROUTE (no IPv4 route found for a
    /// packet), DROP_TTL_EXPIRE (a packet was dropped due to an IPv4
    /// TTL field decremented and reaching zero), and
    /// DROP_BAD_CHECKSUM (a packet had bad IPv4 header checksum and
    /// had to be dropped).
    //std::vector<uint32_t> packetsDropped; // packetsDropped[reasonCode] => number of dropped packets

    /// This attribute also tracks the number of lost bytes.  See also
    /// comment in attribute packetsDropped.
    //std::vector<uint64_t> bytesDropped; // bytesDropped[reasonCode] => number of dropped bytes
    Histogram flowInterruptionsHistogram;             // histogram of durations of flow interruptions
  };

  typedef std::map<FlowId, FlowStats> Stats;

  void AddPacketStats (FlowId flowId, uint32_t packetSize, Time delay, Time maxDelay, uint16_t mrnti);
  void AddPacketStatsTx (FlowId flowId, uint32_t packetSize, Time txTime);
  void AddPacketStatsForwarding (FlowId flowId, uint32_t packetSize, Time forwardingTime);

  void AddPacketDropStats (FlowId flowId, uint32_t packetSize,
                           uint32_t reasonCode);

  /// Get the partial flow statistics stored in this probe.  With this
  /// information you can, for example, find out what is the delay
  /// from the first probe to this one.
  Stats GetStats () const;

  void SerializeToXmlStream (std::ostream &os, int indent,
                             uint32_t index) const;
  FlowStats& GetStatsForProbe (FlowId flowId);

  void GetMetriche (uint32_t flowId);

protected:
  Ptr<MbmsMonitor> m_mbmsMonitor;
  Stats m_stats;
  std::map<FlowId, FlowStats> m_flowStats;

  double m_delayBinWidth;
  double m_jitterBinWidth;
  double m_packetSizeBinWidth;
  double m_flowInterruptionsBinWidth;
  Time m_flowInterruptionsMinTime;

  Statistics m_statistics;

};

} // namespace ns3


#endif /* MBMS_PROBE_H_ */
