/*
 * mbms-probe.cc
 *
 *  Created on: 15/mag/2013
 *      Author: Lorenzo Carl
 */

/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
//
// Copyright (c) 2009 INESC Porto
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation;
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Author: Gustavo J. A. M. Carneiro  <gjc@inescporto.pt> <gjcarneiro@gmail.com>
//              Lorenzo Carla' <lorenzo.carl9@gmai.com>
//
#include "ns3/mbms-probe.h"
#include "ns3/mbms-monitor.h"
#include "ns3/simulator.h"

namespace ns3 {

MbmsProbe::~MbmsProbe ()
{
}

MbmsProbe::MbmsProbe (Ptr<MbmsMonitor> MbmsMonitor)
  : m_mbmsMonitor (MbmsMonitor)
{
  m_mbmsMonitor->AddProbe (this);
}

void MbmsProbe::AddPacketStats (FlowId flowId, uint32_t packetSize, Time delay,
                                Time maxDelay, uint16_t mrnti)
{

  FlowStats &stats = m_stats[flowId];
  Time now = Simulator::Now ();

  //Parametri ritardo e jitter
  stats.delaySum += delay;
  stats.delayHistogram.AddValue (delay.GetSeconds ());
  if (stats.rxPackets > 0)
    {
      Time jitter = stats.lastDelay - delay;
      if (jitter > Seconds (0))
        {
          stats.jitterSum += jitter;
          stats.jitterHistogram.AddValue (jitter.GetSeconds ());
        }
      else
        {
          stats.jitterSum -= jitter;
          stats.jitterHistogram.AddValue (-jitter.GetSeconds ());
        }
    }
  stats.lastDelay = delay;
  stats.mrnti = mrnti;
  if (delay > maxDelay)
    {
      stats.lostPackets++;
    }
  else
    {

      stats.rxBytes += packetSize;
      stats.packetSizeHistogram.AddValue ((double) packetSize);
      stats.rxPackets++;
      if (stats.rxPackets == 1)
        {
          stats.timeFirstRxPacket = now;
        }
      else
        {
          // measure possible flow interruptions
          Time interArrivalTime = now - stats.timeLastRxPacket;
          if (interArrivalTime > m_flowInterruptionsMinTime)
            {
              stats.flowInterruptionsHistogram.AddValue (
                interArrivalTime.GetSeconds ());
            }
        }
      stats.timeLastRxPacket = now;
      // stats.timesForwarded += tracked->second.timesForwarded;
    }
  /*
   FlowStats &flow = m_stats[flowId];
   flow.delayFromFirstProbeSum += delayFromFirstProbe;
   flow.bytes += packetSize;
   ++flow.packets;
   */
}

void MbmsProbe::AddPacketStatsTx (FlowId flowId, uint32_t packetSize,
                                  Time txTime)
{
  FlowStats &stats = m_stats[flowId];
  stats.txBytes += packetSize;
  stats.txPackets++;
  if (stats.txPackets == 1)
    {
      stats.timeFirstTxPacket = txTime;
    }
  stats.timeLastTxPacket = txTime;
}

void MbmsProbe::AddPacketDropStats (FlowId flowId, uint32_t packetSize,
                                    uint32_t reasonCode)
{
  FlowStats &flow = m_stats[flowId];

  if (flow.packetsDropped.size () < reasonCode + 1)
    {
      flow.packetsDropped.resize (reasonCode + 1, 0);
      flow.bytesDropped.resize (reasonCode + 1, 0);
    }
  ++flow.packetsDropped[reasonCode];
  flow.bytesDropped[reasonCode] += packetSize;
}

MbmsProbe::Stats MbmsProbe::GetStats () const
{
  return m_stats;
}



void MbmsProbe::SerializeToXmlStream (std::ostream &os, int indent,
                                      uint32_t index) const
{
#define INDENT(level) for (int __xpto = 0; __xpto < level; __xpto++) {os << ' '; \
  }

  INDENT (indent);
  os << "<MbmsProbe index=\"" << index << "\">\n";

  indent += 2;

  for (Stats::const_iterator iter = m_stats.begin (); iter != m_stats.end ();
       iter++)
    {
      //Delay Probe
      double delay = iter->second.delaySum.GetDouble () /  (double) iter->second.rxPackets;

      //Jitter probe
      double jitter = iter->second.jitterSum.GetDouble () / (double) (iter->second.rxPackets - 1.0);

      //throughputx RX probe
      double throughput = (iter->second.rxBytes * 8) / (iter->second.timeLastTxPacket.GetSeconds ()
                                                        - iter->second.timeFirstTxPacket.GetSeconds ()) / 1024;

      //Percentuale di dropping del probe
      double dropping = 100 * (1.0 - ((double) iter->second.rxPackets / (double) iter->second.txPackets));

      INDENT (indent);
      os << "<FlowStats " << " flowId=\"" << iter->first << "\""
         << " packets=\"" << iter->second.packets << "\"" << " bytes=\""
         << iter->second.bytes << "\"" << " delayFromFirstProbeSum=\""
         << iter->second.delayFromFirstProbeSum << "\""
         << " TimeFirstTxPacket=\"" << iter->second.timeFirstTxPacket
         << "\"" << " TimeFirstRxPacket=\""
         << iter->second.timeFirstRxPacket << "\""
         << " timeLastTxPacket=\"" << iter->second.timeLastTxPacket
         << "\"" << " timeLastRxPacket=\""
         << iter->second.timeLastRxPacket << "\"" << " delaySum=\""
         << iter->second.delaySum << "\"" << " jitterSum=\""
         << iter->second.jitterSum << "\"" << " LastDelay=\""
         << iter->second.lastDelay << "\"" << " TxBytes=\""
         << iter->second.txBytes << "\"" << " RxBytes=\""
         << iter->second.rxBytes << "\"" << " TxPackets=\""
         << iter->second.txPackets << "\"" << " RxPackets=\""
         << iter->second.rxPackets << "\"" << " LostPackets=\""
         << iter->second.lostPackets << "\"" << " TimesForwarded=\""
         << iter->second.timesForwarded << "\""
         << " Throughput=\"" << throughput << " kbps\""
         << " Delay=\"" << delay << " ns\""
         << " Dropping=\"" << dropping << " %\""
         << " Jitter=\"" << jitter << " ns\""
         << " >\n";

      indent += 2;
      for (uint32_t reasonCode = 0;
           reasonCode < iter->second.packetsDropped.size (); reasonCode++)
        {
          INDENT (indent);
          os << "<packetsDropped reasonCode=\"" << reasonCode << "\""
             << " number=\"" << iter->second.packetsDropped[reasonCode]
             << "\" />\n";
        }
      for (uint32_t reasonCode = 0;
           reasonCode < iter->second.bytesDropped.size (); reasonCode++)
        {
          INDENT (indent);
          os << "<bytesDropped reasonCode=\"" << reasonCode << "\""
             << " bytes=\"" << iter->second.bytesDropped[reasonCode]
             << "\" />\n";
        }
      indent -= 2;
      INDENT (indent);
      os << "</FlowStats>\n";
    }
  indent -= 2;
  INDENT (indent);
  os << "</MbmsProbe>\n";
}

inline MbmsProbe::FlowStats&
MbmsProbe::GetStatsForProbe (FlowId flowId)
{
  std::map<FlowId, FlowStats>::iterator iter;
  iter = m_flowStats.find (flowId);
  if (iter == m_flowStats.end ())
    {
      MbmsProbe::FlowStats &ref = m_flowStats[flowId];
      ref.delaySum = Seconds (0);
      ref.jitterSum = Seconds (0);
      ref.lastDelay = Seconds (0);
      ref.txBytes = 0;
      ref.rxBytes = 0;
      ref.txPackets = 0;
      ref.rxPackets = 0;
      ref.lostPackets = 0;
      ref.timesForwarded = 0;
      ref.delayHistogram.SetDefaultBinWidth (m_delayBinWidth);
      ref.jitterHistogram.SetDefaultBinWidth (m_jitterBinWidth);
      ref.packetSizeHistogram.SetDefaultBinWidth (m_packetSizeBinWidth);
      ref.flowInterruptionsHistogram.SetDefaultBinWidth (
        m_flowInterruptionsBinWidth);
      return ref;
    }
  else
    {
      return iter->second;
    }
}

} // namespace ns3




