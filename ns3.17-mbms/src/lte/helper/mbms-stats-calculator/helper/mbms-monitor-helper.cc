// -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*-
//
// Copyright (c) 2009 INESC Porto
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation;
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Author: Gustavo J. A. M. Carneiro  <gjc@inescporto.pt> <gjcarneiro@gmail.com>
//              Lorenzo Carla' <lorenzo.carl9@gmail.com>

#include "mbms-monitor-helper.h"

#include "ns3/mbms-monitor.h"
#include "ns3/mbms-ipv4-flow-classifier.h"
#include "ns3/mbms-ipv4-flow-probe.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/node.h"
#include "ns3/node-list.h"


namespace ns3 {

MbmsMonitorHelper::MbmsMonitorHelper ()
{
  m_monitorFactory.SetTypeId ("ns3::MbmsMonitor");
}

void
MbmsMonitorHelper::SetMonitorAttribute (std::string n1, const AttributeValue &v1)
{
  m_monitorFactory.Set (n1, v1);
}


Ptr<MbmsMonitor>
MbmsMonitorHelper::GetMonitor ()
{
  if (!m_mbmsMonitor)
    {
      m_mbmsMonitor = m_monitorFactory.Create<MbmsMonitor> ();
      m_mbmsClassifier = Create<MbmsIpv4FlowClassifier> ();
      m_mbmsMonitor->SetFlowClassifier (m_mbmsClassifier);
    }
  return m_mbmsMonitor;
}


Ptr<MbmsClassifier>
MbmsMonitorHelper::GetClassifier ()
{
  if (!m_mbmsClassifier)
    {
      m_mbmsClassifier = Create<MbmsIpv4FlowClassifier> ();
    }
  return m_mbmsClassifier;
}


Ptr<MbmsMonitor>
MbmsMonitorHelper::InstallUe (Ptr<Node> node)
{
  Ptr<MbmsMonitor> monitor = GetMonitor ();
  Ptr<MbmsClassifier> classifier = GetClassifier ();
  Ptr<MbmsIpv4FlowProbe> probe = Create<MbmsIpv4FlowProbe> (monitor,
                                                            DynamicCast<MbmsIpv4FlowClassifier> (classifier),
                                                            node);
  return m_mbmsMonitor;
}


Ptr<MbmsMonitor>
MbmsMonitorHelper::InstallUes (NodeContainer nodes)
{
  for (NodeContainer::Iterator i = nodes.Begin (); i != nodes.End (); ++i)
    {
      Ptr<Node> node = *i;
      if (node->GetObject<Ipv4L3Protocol> ())
        {
          InstallUe (node);
        }
    }
  return m_mbmsMonitor;
}

Ptr<MbmsMonitor>
MbmsMonitorHelper::InstallSourceMbms (Ptr<Node> source, std::map <Ipv4Address, uint16_t > Ipv4Mrnti)
{
  Ptr<MbmsMonitor> monitor = GetMonitor ();
  Ptr<MbmsClassifier> classifier = GetClassifier ();
  Ptr<MbmsIpv4FlowProbe> probe = Create<MbmsIpv4FlowProbe> (monitor,
                                                            DynamicCast<MbmsIpv4FlowClassifier> (classifier),
                                                            source);
  probe->SetIpv4Mrnti (Ipv4Mrnti);
  return m_mbmsMonitor;
}

Ptr<MbmsMonitor>
MbmsMonitorHelper::InstallSourcesMbms (NodeContainer sources, std::map <Ipv4Address, uint16_t > Ipv4Mrnti)
{
  for (NodeContainer::Iterator i = sources.Begin (); i != sources.End (); ++i)
    {
      Ptr<Node> node = *i;
      if (node->GetObject<Ipv4L3Protocol> ())
        {
          InstallSourceMbms (node, Ipv4Mrnti);
        }
    }
  return m_mbmsMonitor;
}

Ptr<MbmsMonitor>
MbmsMonitorHelper::SetMapMrntiUe (uint16_t mrnti, std::set <uint16_t> rntiMap)
{
//  std::map <uint16_t, std::set <uint16_t> >::iterator it = m_mapMrntiUe.find (mrnti);
  m_mapMrntiUe.insert (std::pair <uint16_t, std::set <uint16_t> > (mrnti, rntiMap));

  return m_mbmsMonitor;
}


} // namespace ns3



