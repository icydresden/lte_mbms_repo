/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */
#ifndef EPC_Sm_SAP_H
#define EPC_Sm_SAP_H

#include <ns3/address.h>
#include <ns3/ptr.h>
#include <ns3/object.h>
#include <ns3/eps-bearer.h>
#include <ns3/epc-tft.h>
#include <list>
namespace ns3 {

class EpcSmSap
{
public:
  virtual ~EpcSmSap ();


  struct GtpcMessage
  {
    uint32_t teid;
  };

  /**
   * Fully-qualified TEID, see 3GPP TS 29.274 section 8.22
   *
   */
  struct Fteid
  {
    uint32_t teid;
    Ipv4Address address;
  };
};


/**
 * \ingroup lte
 *
 * MME side of the Sm Service Access Point (SAP), provides the MME
 * methods to be called when an Sm message is received by the MME.
 */
class EpcSmSapMme : public EpcSmSap
{
public:

  /**
   * Create Session Request message, see 3GPP TS 29.274 7.13.1
   */
  struct CreateMbmsSessionRequestMessage : public GtpcMessage
  {
    uint32_t teid;
    uint8_t mbmsSessionIdentifier;
    uint16_t mbmsServiceArea;
    EpsBearer bearerLevelQos;
    Ptr<EpcTft> tft;
    Ipv4Address multicastAddress;
    uint64_t tmgi;
  };


  virtual void MbmsSessionStartRequest (CreateMbmsSessionRequestMessage msg) = 0;

};

/**
 * \ingroup lte
 *
 * MBMS-GW side of the Sm Service Access Point (SAP), provides the MBMS-GW
 * methods to be called when an Sm message is received by the MBMS-GW.
 */
class EpcSmSapMbmsGw : public EpcSmSap
{
public:
  /**
     * Create Session Request message, see 3GPP TS 29.274 7.13.2
     */
  struct MbmsSessionResponseMessage : public GtpcMessage
  {
    EpcSmSap::Fteid sgwFteid;
  };


  virtual void MbmsSessionStartResponse (MbmsSessionResponseMessage msg) = 0;

};

/**
 * Template for the implementation of the EpcSmSapMme as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcSmSapMme : public EpcSmSapMme
{
public:
  MemberEpcSmSapMme (C* owner);

  //Inherited from EpcSmSapMme
  virtual void MbmsSessionStartRequest (CreateMbmsSessionRequestMessage msg);


private:
  MemberEpcSmSapMme ();
  C* m_owner;

};

template <class C>
MemberEpcSmSapMme<C>::MemberEpcSmSapMme (C* owner)
  : m_owner (owner)
{
}

template <class C>
MemberEpcSmSapMme<C>::MemberEpcSmSapMme ()
{
}

template <class C>
void MemberEpcSmSapMme<C>::MbmsSessionStartRequest (CreateMbmsSessionRequestMessage msg)
{
  m_owner->DoMbmsSessionStartRequest (msg);
}

/**
 * Template for the implementation of the EpcS1apSapMbmsGw as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcSmSapMbmsGw : public EpcSmSapMbmsGw
{
public:
  MemberEpcSmSapMbmsGw (C* owner);

  //Inherited from EpcSmSapMbmsgw
  virtual void MbmsSessionStartResponse (MbmsSessionResponseMessage msg);

private:
  MemberEpcSmSapMbmsGw ();
  C* m_owner;
};

template <class C>
MemberEpcSmSapMbmsGw<C>::MemberEpcSmSapMbmsGw (C* owner)
  : m_owner (owner)
{
}

template <class C>
MemberEpcSmSapMbmsGw<C>::MemberEpcSmSapMbmsGw ()
{
}

template <class C>
void MemberEpcSmSapMbmsGw<C>::MbmsSessionStartResponse (MbmsSessionResponseMessage msg)
{
  m_owner->DoMbmsSessionStartResponse (msg);
}


} //namespace ns3

#endif /* EPC_Sm_SAP_H_ */
