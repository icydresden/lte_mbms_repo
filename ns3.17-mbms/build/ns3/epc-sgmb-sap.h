/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 Univerista' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */

#ifndef EPC_SGMB_SAP_H_
#define EPC_SGMB_SAP_H_

#include <ns3/address.h>
#include <ns3/ptr.h>
#include <ns3/object.h>
#include <ns3/eps-bearer.h>
#include <ns3/epc-tft.h>
#include <list>

namespace ns3 {

class EpcSgmbSap
{
public:
  virtual ~EpcSgmbSap ();
};


/**
 * \ingroup lte
 *
 * BM-SC side of the Sgmb Service Access Point (SAP), provides the BM-SC
 * methods to be called when an SG-imb message is received by the BM-SC.
 */
class EpcSgmbSapBmsc: public EpcSgmbSap
{
public:
  /**
    *
    * Response to the setup of a MBMS Data Radio Bearer - see 29.061 20 & 29.060 7.5A2.6
    */
   virtual void MbmsSessionStartResponse (uint16_t tmgi) = 0;

};

/**
 * \ingroup lte
 *
 * MBMS-GW side of the Sgmb Service Access Point (SAP), provides the MBMS-GW
 * methods to be called when an Sgmb message is received by the MBMS-GW.
 */
class EpcSgmbSapMbmsGw: public EpcSgmbSap
{
public:
  /**
   * Parameters passed to SessionStartRequest - see 29.061 20 & 29.060 7.5A2.5
   *
   */
  struct MbmsBearerSessionStartRequestParameters
  {
    uint16_t tmgi;
    uint8_t mbmsSessionIdentifier;
    uint16_t mbmsServiceArea;
    Ipv4Address multicastAddress;
    uint32_t gtpTeid;
    EpsBearer bearerLevelQos;
    Ptr<EpcTft> tft;
  };

  /**
   *
   * Request the setup for a MBMS Data Radio Bearer
   */
  virtual void MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params) = 0;
};

/**
 * Template for the implementation of the EpcSgmbSapBmsc as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcSgmbSapBmsc : public EpcSgmbSapBmsc
{
public:
  MemberEpcSgmbSapBmsc (C* owner);

  //Inherited from EpcSgmbSapPgw
  virtual void MbmsSessionStartResponse (uint16_t tmgi);

private:
  MemberEpcSgmbSapBmsc ();
  C* m_owner;

};

template <class C>
MemberEpcSgmbSapBmsc<C>::MemberEpcSgmbSapBmsc (C* owner)
  :m_owner(owner)
{
}

template <class C>
MemberEpcSgmbSapBmsc<C>::MemberEpcSgmbSapBmsc ()
{
}

template <class C>
void MemberEpcSgmbSapBmsc<C>::MbmsSessionStartResponse (uint16_t tmgi)
{
  m_owner->DoMbmsSessionStartResponse (tmgi);
}

/**
 * Template for the implementation of the EpcS1apSapMbmsGw as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcSgmbSapMbmsGw : public EpcSgmbSapMbmsGw
{
public:
  MemberEpcSgmbSapMbmsGw (C* owner);

  //Inherited from EpcSgmbSapMbmsGw
  virtual void MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params);

private:
  MemberEpcSgmbSapMbmsGw ();
  C* m_owner;

};

template <class C>
MemberEpcSgmbSapMbmsGw<C>::MemberEpcSgmbSapMbmsGw (C* owner)
  :m_owner(owner)
 {
 }

template <class C>
MemberEpcSgmbSapMbmsGw<C>::MemberEpcSgmbSapMbmsGw ()
{
}

template <class C>
void MemberEpcSgmbSapMbmsGw<C>::MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params)
{
  m_owner->DoMbmsSessionStartRequest (params);
}

} //namespace ns3

#endif /* EPC_SGMB_SAP_H_ */
