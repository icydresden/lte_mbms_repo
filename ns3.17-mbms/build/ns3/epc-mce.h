/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *      Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */

#ifndef EPC_MCE_H
#define EPC_MCE_H

#include <ns3/object.h>
#include "epc-m2-sap.h"
#include "epc-m3-sap.h"
#include <map>

namespace ns3 {

class EpcMce : public Object
{
  friend class MemberEpcM3SapMce <EpcMce>;
  friend class MemberEpcM2SapMce <EpcMce>;


public:
	 /**
	   * Constructor
	   */
	  EpcMce ();

	  /**
	   * Destructor
	   */
	  virtual ~EpcMce ();

	  // inherited from Object
	    static TypeId GetTypeId (void);
	  protected:
	    virtual void DoDispose ();

	  public:
	  /**
	   * Set the eNB side of the M2 SAP
	   */
	  void SetM2SapEnb (EpcM2SapEnb * s);

	  /**
	   * return the MCE side of the M2 SAP
	   */
	  EpcM2SapMce* GetM2SapMce ();


	  /**
	      * Set the MME side of the M3 SAP
	      *
	      * \params s the MME side of the M3 SAP
	      */
	     void SetM3SapMme (EpcM3SapMme * s);

	     /**
	      * return the MCE side of M3 SAP
	      */
	     EpcM3SapMce* GetM3SapMce ();

	  /**
	   * Add a new ENB to the MME.
	   * \param cellId the unique identifier of the UE
	   * \param enbS1apSap the ENB side of the S1-AP SAP
	   */
	  void AddEnb (uint16_t egci, EpcM2SapEnb* enbM2Sap);


	  // M3 SAP MCE forwarded methods
	  void DoMbmsSessionStartRequest (EpcM3SapMce::MbmsBearerSessionStartRequestParameters msg);

	  // M2 SAP MCE forwarded methods
	  void DoMbmsSessionStartResponse (uint16_t tmgi);


	  /**
	   * Hold info on a ENB
	   *
	   */
	  struct EnbInfo : public SimpleRefCount<EnbInfo>
	  {
	    uint16_t gci;
	    EpcM2SapEnb* m2SapEnb;
	  };

	  /**
	   * EnbInfo stored by EGCI
	   *
	   */
	  std::map<uint16_t, Ptr<EnbInfo> > m_enbInfoMap;

    /**
         * eNB side of M2
         */
        EpcM2SapEnb* m_m2SapEnb;

    /**
     * MCE side of M2
     */
    EpcM2SapMce* m_m2SapMce;


	  /**
	   * MME side of M3
	   */
	  EpcM3SapMme* m_m3SapMme;

	  /**
	   * MCE side of M3
	   */
	  EpcM3SapMce* m_m3SapMce;




};

}

#endif /* EPC_MCE_H_ */
