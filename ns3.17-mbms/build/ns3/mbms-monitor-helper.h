// -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*-
//
// Copyright (c) 2009 INESC Porto
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation;
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// Author: Gustavo J. A. M. Carneiro  <gjc@inescporto.pt> <gjcarneiro@gmail.com>
//              Lorenzo Carla' <lorenzo.carl9@gmail.com>

#ifndef MBMS_MONITOR_HELPER_H
#define MBMS_MONITOR_HELPER_H


#include "ns3/node-container.h"
#include "ns3/object-factory.h"
#include "ns3/mbms-monitor.h"
#include "ns3/mbms-classifier.h"
#include <string>
#include <set>

namespace ns3 {

class AttributeValue;
class MbmsIpv4FlowClassifier;

/// \brief Helper to enable IPv4 flow monitoring on a set of Nodes
class MbmsMonitorHelper
{
public:
  /// \brief Construct a MbmsMonitorHelper class which makes it easier to
  /// configure and use the MbmsMonitor
  MbmsMonitorHelper ();

  /// \brief Set an attribute for the to-be-created MbmsMonitor object
  void SetMonitorAttribute (std::string n1, const AttributeValue &v1);


  /// \param nodes A NodeContainer holding the set of nodes to work with.
  Ptr<MbmsMonitor> InstallSourcesMbms (NodeContainer sources,   std::map <Ipv4Address, uint16_t > Ipv4Mrnti);

  /// \brief Enable flow monitoring on a set of nodes
  /// \param node A Ptr<Node> to the source of MBMS traffic.
  Ptr<MbmsMonitor> InstallSourceMbms (Ptr<Node> source, std::map <Ipv4Address, uint16_t > Ipv4Mrnti);

  /// \param nodes A NodeContainer holding the set of nodes to work with.
  Ptr<MbmsMonitor> InstallUes (NodeContainer nodes);
  /// \brief Enable flow monitoring on a single node
  /// \param node A Ptr<Node> to the node on which to enable flow monitoring of MBMS traffic.
  Ptr<MbmsMonitor> InstallUe (Ptr<Node> node);

  /// \brief Retrieve the MbmsMonitor object created by the Install* methods
  Ptr<MbmsMonitor> GetMonitor ();

  /// \brief Retrieve the MbmsClassifier object created by the Install* methods
  Ptr<MbmsClassifier> GetClassifier ();

  //Set iniziale della mappa MRNTI - UE associati
  Ptr<MbmsMonitor> SetMapMrntiUe (uint16_t mrnti, std::set <uint16_t> rntiMap);

private:
  ObjectFactory m_monitorFactory;
  Ptr<MbmsMonitor> m_mbmsMonitor;
  Ptr<MbmsClassifier> m_mbmsClassifier;
  std::map <uint16_t, std::set <uint16_t> > m_mapMrntiUe;
};

} // namespace ns3

#endif /* MBMS_MONITOR_HELPER_H_ */
