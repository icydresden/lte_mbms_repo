/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *      Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */

#ifndef EPC_MBMSGW_APPLICATION_H
#define EPC_MBMSGW_APPLICATION_H

#include <ns3/address.h>
#include <ns3/socket.h>
#include <ns3/virtual-net-device.h>
#include <ns3/traced-callback.h>
#include <ns3/callback.h>
#include <ns3/ptr.h>
#include <ns3/object.h>
#include <ns3/application.h>
#include <ns3/net-device-container.h>
#include <ns3/epc-tft.h>
#include <ns3/eps-bearer.h>
#include <ns3/ptr.h>
#include "ns3/epc-mme.h"
#include <ns3/ipv4-address-helper.h>
#include <map>
#include <set>
#include "epc-sm-sap.h"
#include "epc-sgmb-sap.h"

namespace ns3 {

class EpcMbmsgwApplication : public Application
{

  friend class MemberEpcSgmbSapMbmsGw<EpcMbmsgwApplication>;
  friend class MemberEpcSmSapMbmsGw<EpcMbmsgwApplication>;

public:
  // inherited from Object
  static TypeId GetTypeId (void);
  virtual void DoDispose();

  /**
   * Constructor
   *
   * \param lteSocket the socket to be used to send/receive packets to/from the LTE radio interface
   * \param s1uSocket the socket to be used to send/receive packets
   * to/from the S1-U interface connected with the SGW
   * \param sgwAddress the IPv4 address at which this eNB will be able to reach its SGW
   *
   */
  EpcMbmsgwApplication(Ptr<Socket> sgimbSocket, Ptr<Socket> m1Socket);

  virtual ~EpcMbmsgwApplication (void);

  struct EpsFlowMbmsId_t
  {
    uint16_t  m_mrnti;
    uint8_t   m_bid;

  public:
    EpsFlowMbmsId_t ();
    EpsFlowMbmsId_t (const uint16_t a, const uint8_t b);

    friend bool operator == (const EpsFlowMbmsId_t &a, const EpsFlowMbmsId_t &b);
    friend bool operator < (const EpsFlowMbmsId_t &a, const EpsFlowMbmsId_t &b);
  };


  /**
   * Set the BM-SC side of the Sgmb SAP
   * \param s the BM-SC side of the Sgmb SAP
   */
  void SetSgmbSapBmSc (EpcSgmbSapBmsc* s);

  /**
   * Return the MBMS-GW side of the Sgmb SAP
   */
  EpcSgmbSapMbmsGw* GetSgmbMbmsGw ();

  /**
   * Set the MME side of the Sm SAP
   * \param s the MME side of the Sm SAP
   */
  void SetSmSapMme (EpcSmSapMme * s);

  /**
   * Return the MBMS-GW side of the Sm SAP
   */
  EpcSmSapMbmsGw* GetSmMbmsGw ();


  /**
   * Method to be assigned to the recv callback of the M1 socket. It is called when the eNB receives a multicast packet from the PGW that is to be forwarded to the UEs.
   *
   * \param socket pointer to the M1socket
   */
  void RecvFromSgimbSocket (Ptr<Socket> socket);

  /**
    * It is called when the MBMS-GW send data packet
    * to eNodeB.
    *
    * \param socket pointer to the m1 socket
    */
   void SendtoM1Socket (Ptr<Packet> packet,
           Ipv4Address enbAddr, uint32_t teid);

  /**
   * Store info MBMS Connection
   */
  void SetMbmsInfoMap (Ipv4Address multiAddress, NetDeviceContainer enbDevices, NetDeviceContainer ueDevices, Ptr<EpcTft> tft, EpsBearer bearer, Ptr<EpcMme> mme);


  /**
   *
   */
  void ReceiveMbmsData (Ptr<Packet> packet);

  // Sm MBMS-GW forwarded methods
  void DoMbmsSessionStartResponse (EpcSmSapMbmsGw::MbmsSessionResponseMessage msg);

  //SGmb MBMS-GW forwarded methods
  void DoMbmsSessionStartRequest (EpcSgmbSapMbmsGw::MbmsBearerSessionStartRequestParameters params);

class MbmsInfo : public SimpleRefCount<MbmsInfo>
{
public:
	MbmsInfo ();

	void SetEnbAddress (Ipv4Address enbAddr);

	Ipv4Address GetEnbAddress ();

private:
	Ipv4Address m_enbAddr;

};

private:

  /**
     * UDP socket to send and receive GTP-U the packets to and from the SG-imb interface
     */
    Ptr<Socket> m_sgimbSocket;

  /**
   * UDP socket to send and receive GTP-U the packets to and from the M1 interface
   */
    Ptr<Socket> m_m1Socket;

   /**
 	* UDP port where the GTP-U Socket is bound
 	*/
	uint16_t m_gtpuUdpPort;

  /**
   * Map telling for each UE address the corresponding MBMS info
   */
    std::map<Ipv4Address, std::set<Ptr<Node> > > m_mbmsInfoMap;

 struct BearerInfoMbms
  {
    Ptr<EpcTft> tft;
    EpsBearer bearer;
    uint8_t bearerId;
  };

 /**
  * Bearer MBMS Counter
  */
 uint8_t m_bearerCounter;
 uint16_t m_lastAllocatedMrnti;
 uint32_t m_cTeidCounter;


 /**
  * Maps of map telling for each MRNTI e BID the corresponding IPv4 Address
  */
 std::map<Ipv4Address, EpsFlowMbmsId_t > m_rbidIpv4Map;

 std::map<Ipv4Address, uint32_t> m_ipv4TeidMap;

 Ptr<EpcMme> m_mme;

 std::list<EpcMme::BearerInfo> m_mbmsBearer;

 /**
  * MBMS-GW side of Sgmb
  */
 EpcSgmbSapMbmsGw* m_sgmbMbmsGw;

 /**
  * BM-SC side of Sgmb
  */
 EpcSgmbSapBmsc* m_sgmbBmsc;

 /**
  * MBMS-GW side of Sm
  */
 EpcSmSapMbmsGw* m_smSapMbmsGw;

 /*
  * MME side of Sm
  */
 EpcSmSapMme* m_smSapMme;

};

}




#endif /* EPC_MBMSGW_APPLICATION_H_ */
