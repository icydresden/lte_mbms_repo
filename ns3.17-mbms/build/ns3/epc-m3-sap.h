/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */
#ifndef EPC_M3_SAP_H
#define EPC_M3_SAP_H

#include <ns3/address.h>
#include <ns3/ptr.h>
#include <ns3/object.h>
#include <ns3/eps-bearer.h>
#include <ns3/epc-tft.h>
#include <list>

namespace ns3 {

class EpcM3Sap
{
public:
	virtual ~EpcM3Sap ();
};


/**
 * \ingroup lte
 *
 * MME side of the M3 Service Access Point (SAP), provides the MME
 * methods to be called when an M3 message is received by the MME.
 */
class EpcM3SapMme: public EpcM3Sap
{
public:

 /**
   *
   * Response to the setup of a MBMS Data Radio Bearer
   */
  virtual void MbmsSessionStartResponse (uint16_t tmgi) = 0;
};

/**
 * \ingroup lte
 *
 * MCE side of the M3 Service Access Point (SAP), provides the MCE
 * methods to be called when an M3 message is received by the MCE.
 */
class EpcM3SapMce: public EpcM3Sap
{
public:
  /**
   * Parameters passed to SessionStartRequest - see 36.444 9.1.3
   *
   */
  struct MbmsBearerSessionStartRequestParameters
  {
    uint16_t tmgi;
    uint8_t mbmsSessionIdentifier;
    uint16_t mbmsServiceArea;
    EpsBearer bearerLevelQos;
    Ptr<EpcTft> tft;
    Ipv4Address multicastAddress;
    uint32_t gtpTeid;
  };

  /**
   *
   * Request the setup for a MBMS Data Radio Bearer
   */
  virtual void MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params) = 0;
};

/**
 * Template for the implementation of the EpcM3SapMme as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcM3SapMme : public EpcM3SapMme
{
public:
  MemberEpcM3SapMme (C* owner);

  //Inherited from EpcM3SapMme
  virtual void MbmsSessionStartResponse (uint16_t tmgi);

private:
  MemberEpcM3SapMme ();
  C* m_owner;

};

template <class C>
MemberEpcM3SapMme<C>::MemberEpcM3SapMme (C* owner)
  :m_owner(owner)
{
}

template <class C>
MemberEpcM3SapMme<C>::MemberEpcM3SapMme ()
{
}

template <class C>
void MemberEpcM3SapMme<C>::MbmsSessionStartResponse (uint16_t tmgi)
{
  m_owner->DoMbmsSessionStartResponse (tmgi);
}



/**
 * Template for the implementation of the EpcS1apSapMce as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcM3SapMce : public EpcM3SapMce
{
public:
  MemberEpcM3SapMce (C* owner);

  //Inherited from EpcM3SapMce
  virtual void MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params);

private:
  MemberEpcM3SapMce ();
  C* m_owner;

};

template <class C>
MemberEpcM3SapMce<C>:: MemberEpcM3SapMce (C* owner)
  :m_owner(owner)
{
}

template <class C>
MemberEpcM3SapMce<C>::MemberEpcM3SapMce ()
{
}

template <class C>
void MemberEpcM3SapMce<C>::MbmsSessionStartRequest (MbmsBearerSessionStartRequestParameters params)
{
  m_owner->DoMbmsSessionStartRequest (params);
}

} //namespace ns3

#endif /* EPC_M3_SAP_H_ */
