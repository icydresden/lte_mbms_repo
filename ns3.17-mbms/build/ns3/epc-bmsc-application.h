/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */

#ifndef EPC_BMSC_APPLICATION_H
#define EPC_BMSC_APPLICATION_H

#include <ns3/address.h>
#include <ns3/ptr.h>
#include <ns3/object.h>
#include <ns3/application.h>
#include <ns3/eps-bearer.h>
#include <ns3/epc-tft.h>
#include <ns3/net-device-container.h>
#include <ns3/epc-sgmb-sap.h>
#include <list>

namespace ns3 {

class EpcBmScApplication : public Application
{

friend class MemberEpcSgmbSapBmsc<EpcBmScApplication>;

public:
  // inherited from Object
  static TypeId GetTypeId (void);
  virtual void DoDispose();

  EpcBmScApplication ();

  virtual ~EpcBmScApplication (void);

  /**
   *
   * Set the MBMS-GW side of the Sgmb
   * \param s the MBMS-GW side of the Sgmb SAP
   */
  void SetSgmbSapMbmsGw (EpcSgmbSapMbmsGw * s);

  /**
   *
   * Return the BM-SC side of Sgmb SAP
   */
  EpcSgmbSapBmsc* GetSgmbSapBmsc ();


  // SGmb SAP forwarded methods
  void DoMbmsSessionStartResponse (uint16_t tmgi);

  /**
   * Start MBMS Session Request
   */
  uint16_t StartMbmsSessionRequest (Ipv4Address multiAddress, NetDeviceContainer enbDevices, Ptr<EpcTft> tft, EpsBearer bearer);

private:

  /**
   * BM-SC side of Sgmb
   */
  EpcSgmbSapBmsc* m_sgmbBmsc;

  /**
   * MBMS-GW side of Sgmb
   */
  EpcSgmbSapMbmsGw* m_sgmbMbmsGw;

  uint16_t m_lastAllocatedTmgi;

};

}

#endif /* EPC_BMSC_APPLICATION_H_ */
