/*
 * lte-mrnti-tag.h
 *
 *  Created on: 19/nov/2012
 *      Author: lorenzo
 */

#ifndef LTE_MRNTI_TAG_H
#define LTE_MRNTI_TAG_H

#include "ns3/tag.h"

namespace ns3 {

/**
 * Questa classe implementa un tag che contiene MRNTI del flusso multicast. Serve in ricezione alla classe flow monitor
 * per la lettura e lo store dei vari flussi
 */

class LteMrntiTag: public Tag {
public:
	LteMrntiTag ();

	void SetMrnti(uint16_t mrnti);
	uint16_t GetMrnti(void) const;

	static TypeId GetTypeId(void);
	virtual TypeId GetInstanceTypeId(void) const;
	virtual uint32_t GetSerializedSize(void) const;
	virtual void Serialize(TagBuffer i) const;
	virtual void Deserialize(TagBuffer i);
	virtual void Print(std::ostream &os) const;



private:
	uint16_t m_mrnti;
};
}

#endif /* LTE_MRNTI_TAG_H_ */
