/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 - Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lorenzo Carla' <lorenzo.carl9@gmail.com>
 */
#ifndef EPC_SGIMB_SAP_H
#define EPC_SGIMB_SAP_H

namespace ns3 {

class EpcSgimbSap
{
public:
	virtual ~EpcSgimbSap ();
};


/**
 * \ingroup lte
 *
 * PGW side of the Sgimb Service Access Point (SAP), provides the PGW
 * methods to be called when an SG-imb message is received by the PGW.
 */
class EpcSgimbSapPgw: public EpcSgimbSap
{
public:
	//TO-DO
};

/**
 * \ingroup lte
 *
 * MBMS-GW side of the SG-imb Service Access Point (SAP), provides the MBMS-GW
 * methods to be called when an Sgimb message is received by the MBMS-GW.
 */
class EpcSgimbSapMbmsGw: public EpcSgimbSap
{
public:
	//TO-DO
};

/**
 * Template for the implementation of the EpcSgimbSapPgw as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcSgimbSapPgw : public EpcSgimbSapPgw
{
public:
  MemberEpcSgimbSapPgw (C* owner);

  //Inherited from EpcSgimbSapPgw

private:
  MemberEpcSgimbSapPgw ();
  C* m_owner;

};


/**
 * Template for the implementation of the EpcS1apSapMbmsGw as a member
 * of an owner class of type C to which all methods are forwarded
 *
 */
template <class C>
class MemberEpcSgimbSapMbmsGw : public EpcSgimbSapMbmsGw
{
public:
  MemberEpcSgimbSapMbmsGw (C* owner);

  //Inherited from EpcSgimbSapMbmsGw

private:
  MemberEpcSgimbSapMbmsGw ();
  C* m_owner;

};



} //namespace ns3

#endif /* EPC_SGIMB_SAP_H */
