//
// Created by lab on 16.07.18.
//

#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ns2-mobility-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/lte-module.h"
#include "ns3/internet-module.h"
using namespace ns3;

int main (int argc, char *argv[])
{



//  std::string filename="DA-circular.ns2mobility";
  std::string filename="DA-straight.ns2mobility";
//  std::string filename="DA-rectangle.ns2mobility";

// Create Ns2MobilityHelper for importing ns-2 format mobility trace
  // DA-straight.ns2mobility.tcl
  Ns2MobilityHelper ns2 = Ns2MobilityHelper (filename+".tcl");

  unsigned int ueNum=3;
// Create Moble nodes.
  NodeContainer ueNodes;
  ueNodes.Create (ueNum);
// configure movements for each node, while reading trace file
  ns2.Install ();

  NodeContainer enbNodes;
  enbNodes.Create(1);

  MobilityHelper mobility;
  mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                "MinX", DoubleValue (0.0),
                                "MinY", DoubleValue (0.0)
  );

  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(enbNodes);

//  NetDeviceContainer ueDevs;
//  NetDeviceContainer enbDevs;
//
//  // IT MUST INSTALL ENBDEVS THEN UEDEVS, WHY? HAVE TO CHECK
////    LteHelper lte;
////    enbDevs=lte.InstallEnbDevice(enbNodes);
////    ueDevs=lte.InstallUeDevice(ueNodes);
////    lte.Attach(ueDevs,enbDevs.Get(0));
//
//  Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();
////    lteHelper->SetAttribute("PathlossModel","ns3::NakagamiPropagationLossModel::m_m0", DoubleValue(1.0));
////    lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));
////    lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::Cost231PropagationLossModel"));
//
//  enbDevs = lteHelper->InstallEnbDevice (enbNodes);
//  ueDevs = lteHelper->InstallUeDevice (ueNodes);
//
//  lteHelper->Attach(ueDevs, enbDevs.Get(0));

//  // Set EnB's position and power
//  Ptr<LteEnbPhy> enbPhy = enbDevs.Get(0)->GetObject<LteEnbNetDevice>()->GetPhy();
//  enbPhy->SetAttribute("TxPower",DoubleValue (43.0));
//  enbPhy->SetAttribute("NoiseFigure", DoubleValue (5.0));

  AnimationInterface anim (filename+".xml");

  Simulator::Stop (Seconds (200));
  Simulator::Run ();
  Simulator::Destroy ();

//   delete ipcs;

  std::cout << "Done." << std::endl;
  return 0;
}